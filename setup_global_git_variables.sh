#!/bin/bash

# Set up global Git settings

read -p "Global user name: " name
read -p "Global user email: " email

echo "==================================="
echo "Configuration before running:"
git config -l
echo "==================================="

git config --global user.email "$email"
git config --global user.name "$name"

echo "==================================="
echo "Configuration after running:"
git config -l
echo "==================================="

echo Configuration done!
