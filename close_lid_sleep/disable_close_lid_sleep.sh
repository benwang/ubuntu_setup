#!/bin/bash
# Used on Ubuntu 20.04


# Back up config file
sudo cp /etc/systemd/logind.conf /etc/systemd/logind.conf.backup
echo Backed up original config file as /etc/systemd/logind.conf.backup

# Uncomment line if commented
sudo sed -i 's/#HandleLidSwitch=/HandleLidSwitch=/' /etc/systemd/logind.conf


# Set HandleLidSwitch to ignore
sudo sed -i 's/HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf
echo Disabled laptop lid close suspend.

# Restart service
read -p "Press enter to restart systemd (This will log you off. Press Ctrl+C to cancel)"
sudo service systemd-logind restart
