#!/bin/bash

required_applications="vim git python3 tmux g++ meld"

# Install required applications
echo Installing required applications: $required_applications
sudo apt install -y $required_applications

# Download ssh setup
echo Setting up SSH keys by downloading the ssh switch utility
wget https://gitlab.com/BADEngineering/badtools/ssh_switch/-/archive/master/ssh_switch-master.zip ~/

