#!/bin/bash

# Instructions: https://docs.docker.com/engine/install/ubuntu/
sudo apt-get update

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo ""
echo ""
echo ""
sudo apt-key fingerprint 0EBFCD88

echo ""
echo "Verify that the fingerprint matches:  9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88"
read -p "Press enter to continue, or Ctrl+C to stop." var

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

echo ""
echo ""
echo ""
read -p "Installation complete. Hit enter to run the docker hello-world!"

sudo docker run hello-world
