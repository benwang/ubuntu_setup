#!/bin/bash
echo Copying file to /etc/udev/rules.d/80-touchscreen.rules
sudo cp 80-touchscreen.rules /etc/udev/rules.d/80-touchscreen.rules

echo Reloading rules without restart...
sudo udevadm control --reload-rules && sudo udevadm trigger

echo Done!

