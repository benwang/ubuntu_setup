#!/bin/bash

themeDir=~/.themes
iconDir=~/.icons

echo Extracting themes to $themeDir

if [ ! -d $themeDir ]; then
	mkdir $themeDir
fi

tar -xf Sweet-Dark.tar.xz -C $themeDir
tar -xf Sweet.tar.xz -C $themeDir

echo Extracting icons to $iconDir

if [ ! -d $iconDir ]; then
	mkdir $iconDir
fi

tar -xf candy-icons.tar.xz -C $iconDir

echo Done! Now open up gnome tweaks and select the themes/icons!
