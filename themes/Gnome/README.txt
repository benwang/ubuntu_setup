Sweet theme and Candy icons

Sweet theme: https://www.gnome-look.org/p/1253385/
Candy icons: https://www.gnome-look.org/p/1305251/


# Sweet theme
Installation:

1.- Extract the zip file to the themes directory i.e. ~/.themes/ or /usr/share/themes/ (create it if necessary).

2.- To set the theme in Gnome, run the following commands in Terminal.

gsettings set org.gnome.desktop.interface gtk-theme Sweet

gsettings set org.gnome.desktop.wm.preferences theme Sweet


or Change via distribution specific tweaktool.

# Candy icons


