Gedit Themes
============

To install a theme:

1) Open gedit
2) Go to Menu > Preferences > Font & Colors
3) Click the + button and select the XML file in this folder
