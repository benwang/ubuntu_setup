#!/bin/bash

# Script to set up Git Extensions on Ubuntu
# Tested on Ubuntu 20.04

download_dir=~/Downloads
install_dir=~/.local/

download_name="GitExtensions-2.51.05-Mono.zip"

echo Temporarily downloading Git Extensions zip to $download_dir


# Version 2.51.05 was final version with Mono support
# https://github.com/gitextensions/gitextensions/releases/tag/v2.51.05
wget -P $download_dir/ https://github.com/gitextensions/gitextensions/releases/download/v2.51.05/$download_name 

# Unzip to ~/.local/
unzip $download_dir/GitExtensions-2.51.05-Mono.zip -d $install_dir
echo Installed to $install_dir


# Remove download files
rm $download_dir/$download_name
echo Removed temporary download file $download_dir/$download_name

# Install mono
sudo apt install -y mono-complete

# Create (copy) desktop shortcut
cp ./shortcuts/GitExtensions.desktop ~/Desktop/


