#!/bin/bash

# Sets up SSH keys for multiple users using the BAD Engineering ssh_switch utility

cd ~/

# Check that files/folders don't already exist
if [[ -f "ssh_switch-master.zip" ]]; then
	echo Error: ~/ssh_switch-master.zip already exists!
	read -p "Remove this file? [Y/n] " remove_file
	if [ "$remove_file" == "Y" ]; then
		rm ~/ssh_switch-master.zip
	else
		echo Did not remove file. Exiting script $0
		exit 1
	fi
fi

if [[ -d "ssh_switch-master" ]]; then
	echo Error: ~/ssh_switch-master/ already exists!
	read -p "Remove this folder? [Y/n] " remove_folder
	if [ "$remove_folder" == "Y" ]; then
		rm -rv ~/ssh_switch-master/
	else
		echo Did not remove folder. Exiting script $0
		exit 1
	fi
fi

# Download ssh setup and extract
echo Setting up SSH keys by downloading the ssh switch utility. Downloading to ~/
wget https://gitlab.com/BADEngineering/badtools/ssh_switch/-/archive/master/ssh_switch-master.zip

echo Downloaded. Unzipping...
unzip ssh_switch-master.zip
echo Done
echo ""
echo "=================================================="
echo "=================================================="
echo "=================================================="

cd ssh_switch-master

############### Handle existing keys ###############
read -p "Use an existing SSH Key? [Y/n] " use_existing_script

while [ "$use_existing_script" == "Y" ]
do
	# Use existing key
	./01_setup_existing.sh
	
	read -p "Use an existing SSH Key? [Y/n] " use_existing_script
done

############### Handle new keys ###############
read -p "Create a new SSH Key? [Y/n] " create_new_key

while [ "$create_new_key" == "Y" ]
do
	# Generate new key
	./02_setup_new.sh
	
	read -p "Create a new SSH Key? [Y/n] " create_new_key
done

############### Install scripts keys ###############
echo Done creating keys. Installing switch scripts
./03_install.sh


############### Clean up ###############
echo Removing the ssh_switch-master.zip from ~/
rm ssh_switch-master.zip

read -p "Remove downloaded ssh_switch-master folder? [Y/n] " create_new_key
if [ "$create_new_key" == "Y" ]; then
	echo Removing the ssh_switch utility from ~/
	rm -rv ssh_switch-master
fi

echo $0 complete!
