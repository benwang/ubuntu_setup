#!/bin/bash

# VirtualBox VM setup

# Add user to vboxsf group to be able to use the VirtualBox
sudo usermod -a -G vboxsf $USER
echo "Added current user to vboxsf group for VirtualBox shares"

